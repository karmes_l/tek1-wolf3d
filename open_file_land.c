/*
** height_wall.c for  in /home/karmes_l/TP/Wolf3D
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Dec 15 17:11:53 2014 lionel karmes
** Last update Tue Dec 23 15:16:55 2014 lionel karmes
*/

#include "my.h"

int		open_file(char *path)
{
  int		fd;

  fd = open(path, O_RDONLY);
  if (fd < 0)
    {
      my_putstr(strerror(errno));
      exit(2);
    }
  return (fd);
}

void		sort_coord(char **str_coord, int y, t_land *land)
{
  int		x;

  x = 0;
  while (str_coord[x] != 0)
    {
      land->tab[y][x] = my_getnbr(str_coord[x]);
      free(str_coord[x]);
      x++;
    }
}

void		print_land(t_land land)
{
  int		y;
  int		x;

  y = SIZE_MAP - 1;
  while (y >= 0)
    {
      x = 0;
      while (x < SIZE_MAP)
	{
	  my_putnbr(land.tab[y][x]);
	  x++;
	}
      my_putchar('\n');
      y--;
    }
}

t_land		init_land()
{
  t_land	land;
  int		y;
  int		fd;
  char		*s;
  char		**str_coord;

  fd = open_file("land");
  y = SIZE_MAP - 1;
  if ((land.tab = malloc(sizeof(int **) * SIZE_MAP)) == NULL)
    exit(0);
  while ((s = get_next_line(fd)) != NULL && y >= 0)
    {
      str_coord = my_strnum_to_wordtab(s);
      if ((land.tab[y] = malloc(sizeof(int *) * SIZE_MAP)) == NULL)
	exit(0);
      sort_coord(str_coord, y, &land);
      y--;
      free(str_coord);
      free(s);
    }
  close(fd);
  return (land);
}
