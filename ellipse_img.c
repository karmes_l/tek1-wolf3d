/*
** ellipste_img.c for  in /home/karmes_l/Projets/Igraph/Wolf3D/v5
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun Dec 28 12:19:13 2014 lionel karmes
** Last update Sun Dec 28 16:44:22 2014 lionel karmes
*/

#include "my.h"

void	ellipse_img(t_data_img *ptr_data_img, t_ellipse *ellipse,
		    int full, t_trigo *trigo)
{
  int	alpha;
  float	x;
  float	y;
  int	k;

  k = 1;
  if (full == 1)
    k = MAX(ellipse->rayon1, ellipse->rayon2);
  while (k > 0)
    {
      alpha = 0;
      while (alpha < 360)
	{
	  x = ellipse->point.x + ellipse->rayon1 * trigo->cos[alpha];
	  y = ellipse->point.y + ellipse->rayon2 * trigo->sin[alpha];
	  mlx_pixel_put_image(ptr_data_img, x, y, ellipse->point.color);
	  mlx_pixel_put_image(ptr_data_img, x + 1, y, ellipse->point.color);
	  mlx_pixel_put_image(ptr_data_img, x + 1, y + 1, ellipse->point.color);
	  mlx_pixel_put_image(ptr_data_img, x, y + 1, ellipse->point.color);
	  alpha += 360 / (8 * MAX(ellipse->rayon1, ellipse->rayon2)) + 1;
	}
      k--;
      (ellipse->rayon1 > 0) ? (ellipse->rayon1--) : (k = 0);
      (ellipse->rayon2 > 0) ? (ellipse->rayon2--) : (k = 0);
    }
}
