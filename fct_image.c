/*
** image.c for  in /home/karmes_l/Projets/FDF/test
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Nov  8 10:13:45 2014 lionel karmes
** Last update Wed Dec 24 15:20:34 2014 lionel karmes
*/

#include "my.h"

t_data_img	init_data_img(void *img_ptr, char *data, int bits_per_pixel,
			      int size_line)
{
  t_data_img	ptr_data_img;

  ptr_data_img.img_ptr = img_ptr;
  ptr_data_img.data = data;
  ptr_data_img.bits_per_pixel = bits_per_pixel;
  ptr_data_img.size_line = size_line;
  return (ptr_data_img);
}

void		mlx_pixel_put_image(t_data_img *ptr_data_img, int x, int y,
				    int color)
{
  unsigned long	pixel;

  if (x < SIZE_WIN_X && x >= 0 && y < SIZE_WIN_Y && y >= 0)
    {
      pixel = x * (ptr_data_img->bits_per_pixel / 8)
	+ ptr_data_img->size_line * y;
      ptr_data_img->data[pixel] = color;
      ptr_data_img->data[pixel + 1] = color >> 8;
      ptr_data_img->data[pixel + 2] = color >> 16;
    }
}

void		image(t_win_img *img)
{
  void		*img_ptr;
  char		*data;
  int		bits_per_pixel;
  int		size_line;
  int		endian;
  t_data_img	ptr_data_img;

  if ((img_ptr = mlx_new_image(img->mlx_ptr, img->x, img->y)) == NULL)
    exit(1);
  if ((data = mlx_get_data_addr(img_ptr, &bits_per_pixel, &size_line, &endian)) == NULL)
    exit(1);
  ptr_data_img = init_data_img(img_ptr, data, bits_per_pixel, size_line);
  img->data_img = ptr_data_img;
}
