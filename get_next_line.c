/*
** get_next_line.c for  in /home/karmes_l/Projets/Prog_Elem/get_next_line
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Nov 15 14:26:23 2014 lionel karmes
** Last update Fri Jan  2 11:57:17 2015 lionel karmes
*/

#include "my.h"

int		end_line(char *str, int c)
{
  int		i;

  i = 0;
  while (str[i + c] != 0)
    {
      if (str[i + c] == '\n')
	return (i);
      i++;
    }
  return (i);
}

char		*update_buff(char *buff, char *str, int c, long size_str_line)
{
  char		*str_line;
  int		i;
  int		j;

  size_str_line += c;
  if ((str_line = malloc(sizeof(char) * (size_str_line + 1))) != NULL)
    {
      str_line[size_str_line] = '\0';
      i = 0;
      while (i < size_str_line - c)
	{
	  str_line[i] = buff[i];
	  i++;
	}
      if (size_str_line - c != 0)
	free(buff);
      j = 0;
      while (j < c)
	{
	  str_line[i + j] = str[j];
	  j++;
	}
    }
  return (str_line);
}

char		*update_buff2(char *str, int c)
{
  char		*str_line;
  int		i;

  i = 0;
  if ((str_line = malloc(sizeof(char) * (end_line(str, c) + 1))) != NULL)
    {
      str_line[end_line(str, c)] = '\0';
      while (i < end_line(str, c))
	{
	  str_line[i] = str[c + i];
	  i++;
	}
    }
  return (str_line);
}

int		reading(t_line *ptr_line, char *str, int fd)
{
 long		size_str_line;

 size_str_line = 0;
 if ((ptr_line->str_line = malloc(sizeof(char) * 1)) == NULL)
   return (0);
 ptr_line->str_line[0] = '\0';
 while (ptr_line->c < ptr_line->len)
   {
     ptr_line->str_line = update_buff2(str, ptr_line->c);
     size_str_line = ptr_line->len - ptr_line->c;
     ptr_line->c += end_line(str, ptr_line->c);
     if (ptr_line->c != ptr_line->len)
       return (1);
   }
 while ((ptr_line->len = read(fd, str, SIZE_TO_READ)) > 0)
   {
     str[ptr_line->len] = '\0';
     ptr_line->c = end_line(str, 0);
     ptr_line->str_line = update_buff(ptr_line->str_line,
				      str, ptr_line->c, size_str_line);
     size_str_line += ptr_line->len;
     if (ptr_line->c != ptr_line->len)
       return (1);
   }
 return (0);
}

char		*get_next_line(const int fd)
{
  static char	*str;
  static t_line	ptr_line;

  if (SIZE_TO_READ >= 0 && str == NULL)
    {
      ptr_line.c = 0;
      ptr_line.len = -1;
      str = malloc(sizeof(char) * (SIZE_TO_READ + 1));
      if (str == NULL)
      	return (NULL);
    }
  else if (SIZE_TO_READ < 0 || fd < 0)
    return (NULL);
  if (reading(&ptr_line, str, fd))
    {
      ptr_line.c++;
      return (ptr_line.str_line);
    }
  if (ptr_line.c > 0)
    {
      ptr_line.c = 0;
      return (ptr_line.str_line);
    }
  free(ptr_line.str_line);
  return (NULL);
}
