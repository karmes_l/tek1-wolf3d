##
## Makefile for  in /home/karmes_l/Projets/Maths
## 
## Made by lionel karmes
## Login   <karmes_l@epitech.net>
## 
## Started on  Mon Nov  3 16:51:51 2014 lionel karmes
## Last update Fri Jan  2 11:10:04 2015 lionel karmes
##

CC	= gcc

RM	= rm -f

CFLAGS	+= -Wextra -Wall -Werror
CFLAGS	+= -ansi -pedantic
CFLAGS	+= -I./include/

LDFLAGS	=

NAME	= wolf3D

LIB	= libmy

SRCS	= main.c \
	my_wolf3D.c \
	droite_img.c \
	fct_image.c \
	image.c \
	my_events.c \
	height_wall.c \
	get_next_line.c \
	open_file_land.c \
	move.c \
	color_wall.c \
	rectangle_img.c \
	mini_map.c \
	init_trigo.c \
	ellipse_img.c \
	bad_way.c

OBJS	= $(SRCS:.c=.o)


all: $(NAME)

$(NAME): $(OBJS)
	make -C lib/
	make -C minilibx/
	$(CC) $(OBJS) -o $(NAME) $(LDFLAGS) -L./lib/ -lmy -L./minilibx/ -lmlx -L/usr/lib64/X11 -lXext -lX11 -I./include/ -lm

clean:
	$(RM) $(OBJS)
	make clean -C lib/
	make clean -C minilibx/

fclean: clean
	make fclean -C lib/
	make clean -C minilibx/

	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
