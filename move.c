/*
** move.c for  in /home/karmes_l/Projets/Igraph/Wolf3D/v2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Dec 20 13:36:41 2014 lionel karmes
** Last update Fri Jan  2 11:53:49 2015 lionel karmes
*/

#include "my.h"

int	movement_valid(t_coord *coord, float vect_x1, float vect_y1)
{
  float	k;
  float x1;
  float y1;

  k = 0;
  while (k < 1)
    {
      k += 0.01;
      x1 = coord->position.x0 + k * vect_x1;
      y1 = coord->position.y0 + k * vect_y1;
      if (x1 >= 0 && x1 < SIZE_MAP && y1 >= 0 && y1 < SIZE_MAP)
	{
	  if (coord->land.tab[(int) y1][(int) x1] == 1)
	    return (0);
	}
    }
  coord->position.x0 = x1;
  coord->position.y0 = y1;
  return (1);
}

int	movement(int keycode, t_coord *coord, t_trigo *trigo)
{
  float	foot;
  float	vect_x1;
  float	vect_y1;

  foot = 0.2;
  if (keycode == K_UP)
    {
      vect_x1 = trigo->cos[coord->position.alpha] * foot;
      vect_y1 = trigo->sin[coord->position.alpha] * foot;
    }
  else if (keycode == K_DOWN)
    {
      vect_x1 = - trigo->cos[coord->position.alpha] * foot;
      vect_y1 = - trigo->sin[coord->position.alpha] * foot;
    }
  else
    return (-1);
  if (!movement_valid(coord, vect_x1, vect_y1))
    return (0);
  return (1);
}

void	rotation(int keycode, t_coord *coord)
{
    if (keycode == K_LEFT)
      coord->position.alpha = (coord->position.alpha + 10) % 360;
    if (keycode == K_RIGHT)
      coord->position.alpha = (coord->position.alpha - 10);
    if (coord->position.alpha < 0)
      coord->position.alpha += 360;
}

int	move(int keycode, t_coord *coord, t_trigo *trigo)
{
  if (!movement(keycode, coord, trigo))
    return (0);
  rotation(keycode, coord);
  return (1);
}
