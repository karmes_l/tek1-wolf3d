/*
** mini_map.c for  in /home/karmes_l/Projets/Igraph/Wolf3D/v3
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Dec 22 18:21:35 2014 lionel karmes
** Last update Fri Jan  2 11:31:55 2015 lionel karmes
*/

#include "my.h"

int		mini_map_color_tab(t_position *position, t_land *land, int x, int y)
{
  int		color;

  color = 0xFFFFFF;
  (land->tab[y][x] == 1 ? (color = 0x000000) : (color = color));
  (land->tab[y][x] == 2 ? (color = 0x00FF00) : (color = color));
  ((x == (int) position->x0 && y == (int) position->y0) ?
   (color = 0xFF0000) : (color = color));
  return (color);
}

void		mini_map_tab(t_win_img *img, t_position *position, t_land *land)
{
  t_droite	droite;
  int		y;
  int		x;

  y = SIZE_MAP - 1;
  droite.point1.y = SIZE_WIN_Y / 50;
  droite.point2.y = droite.point1.y + (SIZE_WIN_Y / 5) / SIZE_MAP;
  while (y >= 0)
    {
      x = 0;
      droite.point1.x = SIZE_WIN_X - (SIZE_WIN_X / 5 + SIZE_WIN_X / 50);
      droite.point2.x = droite.point1.x + (SIZE_WIN_X / 5) / SIZE_MAP;
      while (x < SIZE_MAP)
	{
	  rectangle_img(&(img->data_img), &droite,
			mlx_get_color_value(img->mlx_ptr,
					    mini_map_color_tab(position, land, x, y)));
	  droite.point1.x += (SIZE_WIN_Y / 5) / 20;
	  droite.point2.x += (SIZE_WIN_Y / 5) / 20;
	  x++;
	}
      droite.point1.y += (SIZE_WIN_Y / 5) / 20;
      droite.point2.y += (SIZE_WIN_Y / 5) / 20;
      y--;
    }
}

void		mini_map(t_win_img *img, t_position *position, t_land *land)
{
  mini_map_tab(img, position, land);
}
