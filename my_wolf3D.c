/*
** my_wolf3D.c for  in /home/karmes_l/TP/Wolf3D
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Dec 15 10:53:11 2014 lionel karmes
** Last update Fri Jan  2 11:32:10 2015 lionel karmes
*/

#include "my.h"

void		init_all(void *mlx_ptr, t_win_img *img, t_coord *coord,
			 t_trigo *trigo)
{
  t_land	land;
  t_position	position;

  img->mlx_ptr = mlx_ptr;
  img->x = SIZE_WIN_X;
  img->y = SIZE_WIN_Y;
  position.x0 = 3.5;
  position.y0 = 14.5;
  position.alpha = 0;
  land = init_land();
  coord->land = land;
  coord->position = position;
  init_trigo(trigo);
}

void		my_wolf3D()
{
  void		*mlx_ptr;
  void		*win_ptr;
  t_win_img	img;
  t_coord	coord;
  t_trigo	trigo;

  if ((mlx_ptr = mlx_init()) == NULL)
    exit(1);
  if ((win_ptr = mlx_new_window(mlx_ptr, SIZE_WIN_X, SIZE_WIN_Y, "Wolf3D"))
      == NULL)
    exit(1);
  init_all(mlx_ptr, &img, &coord, &trigo);
  image(&img);
  my_events(win_ptr, &img, &coord, &trigo);
}
