/*
** image.c for  in /home/karmes_l/Projets/FDF/test
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Nov  8 10:13:45 2014 lionel karmes
** Last update Fri Jan  2 11:57:25 2015 lionel karmes
*/

#include "my.h"

void		draw_sky(t_win_img *img, t_droite *droite,
			 int height)
{
  droite->point1.y = 0;
  droite->point2.y = SIZE_WIN_Y / 2 - height / 2;
  droite_img(&(img->data_img), droite,
	     mlx_get_color_value(img->mlx_ptr, 0x26C4EC));
}

void		draw_wall(t_win_img *img, t_droite *droite,
			  t_wall wall)
{
  int		height;
  int		color;

  height = wall.height;
  if (height > SIZE_WIN_Y)
    height = SIZE_WIN_Y;
  color = wall.color;
  droite->point1.y = SIZE_WIN_Y / 2 - height / 2;
  droite->point2.y = SIZE_WIN_Y / 2 + height / 2;
  droite_img(&(img->data_img), droite,
	     mlx_get_color_value(img->mlx_ptr, color));
}

void		draw_floor(t_win_img *img, t_droite *droite,
			   int height)
{
  droite->point1.y = SIZE_WIN_Y / 2 + height / 2;
  droite->point2.y = SIZE_WIN_Y;
  droite_img(&(img->data_img), droite,
	     mlx_get_color_value(img->mlx_ptr, 0x3A9D23));
}

void		draw_image(t_win_img *img, t_position *position,
			   t_land *land, t_trigo *trigo)
{
  t_droite	droite;
  t_wall	wall;
  int		x;

  x = 0;
  while (x < SIZE_WIN_X)
    {
      wall = height_wall(x, position, land, trigo);
      droite.point1.x = x;
      droite.point2.x = x;
      draw_sky(img, &droite, wall.height);
      draw_wall(img, &droite, wall);
      draw_floor(img, &droite, wall.height);
      x++;
    }
}
