/*
** height_wall.c for  in /home/karmes_l/TP/Wolf3D
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Dec 15 17:11:53 2014 lionel karmes
** Last update Fri Jan  2 11:56:18 2015 lionel karmes
*/

#include "my.h"

t_wall		wall_found(float x_para_droite, float y_para_droite,
			   float k, t_wall *wall)
{
  wall->color = color_wall(y_para_droite, x_para_droite);
  (k < 1 ? (k = 1) : (k = k));
  wall->height = SIZE_WIN_Y / k;
  return (*wall);
}

t_wall		equation_parametric_droite(float vect_x, float vect_y,
					   t_position *position, t_land *land)
{
  float		k;
  float		x_para_droite;
  float		y_para_droite;
  t_wall	wall;

  k = 0;
  while (position->x0 + k * vect_x < SIZE_MAP &&
	 position->y0 + k * vect_y < SIZE_MAP)
    {
      k += 0.002;
      x_para_droite = position->x0 + k * vect_x;
      y_para_droite = position->y0 + k * vect_y;
      if (((int) position->y0 != (int) y_para_droite ||
	   (int) position->x0 != (int) x_para_droite) &&
	  (x_para_droite >= 0 && y_para_droite >= 0))
	{
	  if (land->tab[(int) y_para_droite][(int) x_para_droite] != 0)
	    return (wall_found(x_para_droite, y_para_droite, k, &wall));
	}
    }
  wall.height = 0;
  wall.color = 0xFFFFFF;
  return (wall);
}

t_wall		height_wall(int x, t_position *position,
			    t_land *land, t_trigo *trigo)
{
  int		len_seg;
  float		len_x0y0_to_seg;
  float		vecteur_x;
  float		vecteur_y;
  float		y1;
  float		x1;

  len_seg = 1;
  len_x0y0_to_seg = 0.5;
  x1 = len_seg;
  y1 = len_x0y0_to_seg * ((float) (SIZE_WIN_X / 2 - x) / SIZE_WIN_X);
  vecteur_x = x1 * trigo->cos[position->alpha] - y1 *
    trigo->sin[position->alpha];
  vecteur_y = x1 * trigo->sin[position->alpha] + y1 *
    trigo->cos[position->alpha];
  return (equation_parametric_droite(vecteur_x, vecteur_y, position, land));
}
