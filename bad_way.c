/*
** bad_way.c for  in /home/karmes_l/Projets/Igraph/Wolf3D/v5
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun Dec 28 12:11:01 2014 lionel karmes
** Last update Sun Dec 28 16:45:13 2014 lionel karmes
*/

#include "my.h"

void		bad_way(t_win_img *img, t_trigo *trigo)
{
  t_ellipse	ellipse;
  t_droite      droite;

  ellipse.point.x = SIZE_WIN_X / 8;
  ellipse.point.y = SIZE_WIN_Y / 8;
  ellipse.rayon1 = SIZE_WIN_X / 10;
  ellipse.rayon2 = SIZE_WIN_Y / 10;
  ellipse.point.color = mlx_get_color_value(img->mlx_ptr, 0xFF0000);
  droite.point1.x = ellipse.point.x - (float) ellipse.rayon1 * 0.75;
  droite.point2.x = ellipse.point.x + (float) ellipse.rayon1 * 0.75;
  droite.point1.y = ellipse.point.y - (float) ellipse.rayon2 * 0.25;
  droite.point2.y = ellipse.point.y + (float) ellipse.rayon2 * 0.25;
  ellipse_img(&(img->data_img), &ellipse, 1, trigo);
  rectangle_img(&(img->data_img), &droite,
		mlx_get_color_value(img->mlx_ptr, 0xFFFFFF));
}
