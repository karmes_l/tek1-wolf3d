/*
** coord.h for  in /home/karmes_l/Projets/FDF/my_fdf/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Dec  3 10:14:39 2014 lionel karmes
** Last update Fri Jan  2 11:59:21 2015 lionel karmes
*/

#ifndef TRIGONOMETRIE_H_
# define TRIGONOMETRIE_H_

typedef struct	s_trigo
{
  float		*cos;
  float		*sin;
}		t_trigo;

#endif /* !TRIGONOMETRIE_H_ */
