/*
** my.h for  in /home/karmes_l/test/tmp_Piscine_C_J09
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Thu Oct  9 15:22:30 2014 lionel karmes
** Last update Sun Dec 28 16:46:21 2014 lionel karmes
*/

#ifndef MY_H_
# define MY_H_

# include <math.h>
# include <errno.h>
# include <string.h>
# include <stdlib.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# include "mlx.h"
# include "get_next_line.h"
# include "coord.h"
# include "figure.h"
# include "trigo.h"
# include "pixel_image.h"
# include "events.h"

char		*convert_base(char *, char *, char *);
int		count_num(long nb);
int		my_charisnum(char);
void		my_putchar(char);
void		my_putnbr(long);
void		my_swap(int *, int *);
void		my_putstr(char *);
int		my_strlen(char *);
int		my_getnbr(char *);
void		my_sort_int_tab(int *, int);
char		*mu_strcpy(char *, char *);
char		*my_strncpy(char *, char *, int);
char		*my_revstr(char *);
char		*my_strstr(char *, char *);
int		my_strcmp(char *, char *);
int		my_strncmp(char *, char *, int n);
char		*my_strupcase(char *);
char		*my_strlowcase(char *);
char		*mystrcapitalize(char *);
int		my_str_isalpha(char *);
int		my_str_isnum(char *);
int		mu_str_islower(char *);
int		my_str_isupper(char *);
int		my_str_isprintable(char *);
char		*my_strcat(char *, char *);
char		*my_strncat(char *, char *, int);
int		my_strlcat(char *, char *, int);
int		my_str_isnum2(char *);
unsigned long	pow_10(int);
int		power(unsigned long, unsigned long);
char		**my_strnum_to_wordtab(char *);
void		my_wolf3D();
void		my_events(void *, t_win_img *, t_coord *, t_trigo *);
void		image(t_win_img *img);
void		draw_image(t_win_img *, t_position *, t_land *, t_trigo *);
t_data_img	init_data_img(void *img_ptr, char *data, int b_p_p, int size_line);
void		mlx_pixel_put_image(t_data_img *ptr_data_img, int x, int y, int color);
void		droite_img(t_data_img *ptr_data_img, t_droite *coord, int color);
int		droite1_img(t_data_img *ptr_data_img, t_droite *coord, int color);
int		droite2_img(t_data_img *ptr_data_img, t_droite *coord, int color);
int		droite3_img(t_data_img *ptr_data_img, t_droite *coord, int color);
void		draw_sky(t_win_img *, t_droite *, int);
void		draw_wall(t_win_img *, t_droite *, t_wall);
void		draw_floor(t_win_img *, t_droite *, int);
t_wall		height_wall(int, t_position *, t_land *, t_trigo *);
t_land		init_land();
void		print_land(t_land land);
int		move(int, t_coord *, t_trigo *);
int		color_wall(float, float);
void		mini_map(t_win_img *, t_position *, t_land *);
void		rectangle_img(t_data_img *, t_droite *, int);
void		ellipse_img(t_data_img *, t_ellipse *, int, t_trigo *);
void		init_trigo(t_trigo *trigo);
void		bad_way(t_win_img *, t_trigo *);

# define SIZE_WIN_X (1000)
# define SIZE_WIN_Y (1000)
# define SIZE_MAP (20)

#endif /* !MY_H_ */
