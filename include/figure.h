/*
** droite.h for  in /home/karmes_l/Projets/FDF/test/include
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Mon Nov 17 12:07:10 2014 lionel karmes
** Last update Sun Dec 28 12:38:42 2014 lionel karmes
*/

#ifndef FIGURE_H_
# define FIGURE_H_

# define PI		(3.14159265359)
# define MAX(v1, v2)	((v1) > (v2) ? (v1) : (v2))
# define MIN(v1, v2)	((v1) < (v2) ? (v1) : (v2))

typedef struct	s_pixel
{
  int		x;
  int		y;
  int		z;
  int		color;
}		t_pixel;

typedef	struct	s_droite
{
  t_pixel	point1;
  t_pixel	point2;
}		t_droite;

typedef struct	s_ellipse
{
  int		rayon1;
  int		rayon2;
  t_pixel	point;
}		t_ellipse;

#endif /* !FIGURE_H_ */
