/*
** events.h for  in /home/karmes_l/Projets/Igraph/Wolf3D/v2
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sat Dec 20 11:47:45 2014 lionel karmes
** Last update Sat Dec 20 11:58:17 2014 lionel karmes
*/

#ifndef EVENTS_H_
# define EVENTS_H_

# define K_ECHAP (65307)
# define K_RIGHT (65363)
# define K_LEFT (65361)
# define K_UP (65362)
# define K_DOWN (65364)

#endif /* !EVENTS_H_ */

