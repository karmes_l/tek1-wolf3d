/*
** coord.h for  in /home/karmes_l/Projets/FDF/my_fdf/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Dec  3 10:14:39 2014 lionel karmes
** Last update Wed Dec 24 15:30:12 2014 lionel karmes
*/

#ifndef COORD_H_
# define COORD_H_

typedef struct	s_land
{
  int		**tab;
}		t_land;

typedef struct	s_position
{
  float		x0;
  float		y0;
  int		alpha;
}		t_position;

typedef struct	s_coord
{
  t_land	land;
  t_position	position;
}		t_coord;

typedef struct	s_wall
{
  int		height;
  int		color;
}		t_wall;

#endif /* !COORD_H_ */
