/*
** coord.h for  in /home/karmes_l/Projets/FDF/my_fdf/v1
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Dec  3 10:14:39 2014 lionel karmes
** Last update Tue Dec 16 12:12:32 2014 lionel karmes
*/

#ifndef _COORD_H_
# define _COORD_H_

typedef struct	s_land
{
  int		**tab;
}		t_land;

typedef struct	s_position
{
  int		x0;
  int		y0;
  float		alpha;
}		t_position;

#endif /* !COORD_H_ */
