/*
** init_trigo.c for  in /home/karmes_l/Projets/Igraph/Wolf3D/v4
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Wed Dec 24 14:54:56 2014 lionel karmes
** Last update Wed Dec 24 15:06:06 2014 lionel karmes
*/

#include "my.h"

void	init_trigo(t_trigo *trigo)
{
  int	i;

  trigo->cos = malloc(sizeof(float) * 360);
  trigo->sin = malloc(sizeof(float) * 360);
  i = 0;
  while (i < 360)
    {
      trigo->cos[i] = cos(i * PI / 180);
      trigo->sin[i] = sin(i * PI / 180);
      i++;
    }
}
