/*
** color_wall.c for  in /home/karmes_l/Projets/Igraph/Wolf3D/v3
** 
** Made by lionel karmes
** Login   <karmes_l@epitech.net>
** 
** Started on  Sun Dec 21 10:25:49 2014 lionel karmes
** Last update Fri Jan  2 11:52:42 2015 lionel karmes
*/

#include "my.h"

int	color_wall(float y_para_droite, float x_para_droite)
{
  int	color;
  int	y;
  int	x;

  y = (int) y_para_droite;
  x = (int) x_para_droite;
  y_para_droite -= y;
  x_para_droite -= x;
  y = y_para_droite * 100;
  x = x_para_droite * 100;
  color = 0xFFFFFF;
  color = ((y >= 90) ? 0xD1B606 : color);
  color = ((y < 10) ? 0xC8AD7F : color);
  color = ((x < 10) ? 0x4E3D28 : color);
  color = ((x >= 90) ? 0xFFF48D : color);
  return (color);
}
